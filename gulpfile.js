const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const { series } = require('gulp');

const paths = {
  styles: {
    src: 'scss/**/*.scss',
    dest: 'css/'
  },
  bootstrap: {
    src: 'node_modules/bootstrap/scss/bootstrap.scss',
    dest: 'css/'
  }
};

function bootstrap() {
  return gulp.src(paths.bootstrap.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.bootstrap.dest));
}

function customStyles() {
  return gulp.src(paths.styles.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.styles.dest));
}

const buildStyles = series(bootstrap, customStyles);

function watch() {
  gulp.watch(paths.bootstrap.src, bootstrap);
  gulp.watch(paths.styles.src, customStyles);
}

exports.buildStyles = buildStyles;
exports.watch = watch;
